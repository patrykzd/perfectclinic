var fs =  require('fs');

fs.readFile('./medycyna.txt','utf-8',function(error, content){

    var table = content.split(/(Medycyna estetyczna|Kosmetologia estetyczna)/gim);
    table.shift();
    for (let index = 0; index < table.length; index++) {
        var table2 = table[index].split(".");
        table[index]=table2;
    }
    table[1].shift();
    table[3].shift();
    table[0][0] = table[0][0].replace(/(.+)/g, "<h3 class=\"my-3\">$1</h3>");
    table[2][0] = table[2][0].replace(/(.+)/g, "<h3 class=\"my-3\">$1</h3>");

    function fixTable(table) {
    
        for (let index = 0; index < table.length; index++) {
            table[index] =  table[index].split(/\n/g);
        }
    
        for (let i = 0; i < table.length; i++) {
            table[i].pop();
        }
    
        for (let i = 0; i < table.length; i++) {
            for (let j = 0; j < table[i].length; j++) {
                var str = table[i][j];
                if(j>0 && j<table[i].length-1){
                    if(str.match(/(od \d{2,4} ?[zł].*|\d{1,4}-\d{2,4} ?[zł].*|\d{2,4}\/\d{2,4} ?[zł].*|\d{2,4} ?[zł].*)/g)!= null){
                        table[i][j] = str.replace(/(od \d{2,4} ?[zł].*|\d{1,4}-\d{2,4} ?[zł].*|\d{2,4}\/\d{2,4} ?[zł].*|\d{2,4} ?[zł].*)/g,"<strong class=\"ml-auto\">$1</strong>");
                        table[i][j] = table[i][j].replace(/(.+)/g,"<li class=\"list-group-item\">$1</li>");
                    } else {
                        table[i][j] = table[i][j].replace(/(.+)/g,"<li class=\"list-group-item\"><strong class=\"ml-auto mr-auto\">$1</strong></li>");
                    }
                } else if(j==0){
                    table[i][j] = table[i][j].replace(/(.+)/g,"<button type=\"button\" class=\"btn-switch btn-block py-1 my-2\">$1</button>\n<ul class=\"px-0 my-1 hidden\">");
                } else {
                    if(str.match(/(od \d{2,4} ?[zł].*|\d{1,4}-\d{2,4} ?[zł].*|\d{2,4}\/\d{2,4} ?[zł].*|\d{2,4} ?[zł].*)/g)!= null){
                        table[i][j] = str.replace(/(od \d{2,4} ?[zł].*|\d{1,4}-\d{2,4} ?[zł].*|\d{2,4}\/\d{2,4} ?[zł].*|\d{2,4} ?[zł].*)/g,"<strong class=\"ml-auto\">$1</strong>");
                        table[i][j] = table[i][j].replace(/(.+)/g,"<li class=\"list-group-item\">$1</li>\n</ul>");
                    } else {
                        table[i][j] = table[i][j].replace(/(.+)/g,"<li class=\"list-group-item\"><strong class=\"ml-auto mr-auto\">$1</strong></li>\n</ul>");
                    }
                }
            }
        }
    }
    
    fixTable(table[1]);

    fixTable(table[3]);

    var tab = table.join("\n");

    tab = tab.replace(/(,|\n)/g,"");

    fs.writeFile('./priceList.txt', tab, function(error){
        if(error){
            console.log(error);
        } else {
            console.log("plik został utworzony");
        }
    });

    fs.readFile('price-list1.html','utf-8',function(error, content){
        content = content.replace(/(\*\*\*\*\*)/g, tab);
        fs.writeFile('price-list.html', content, function(error){
            if(error){
                console.log(error);
            } else {
                console.log("File price-list.html changed");
            }
        });
    });

});